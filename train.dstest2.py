import pandas as pd
import argparse
from model import *
from loss import *
from accuracy import *
from utils import *
import tensorflow as tf

from keras.backend.tensorflow_backend import set_session
config = tf.ConfigProto()
config.gpu_options.allow_growth = True
config.gpu_options.per_process_gpu_memory_fraction = 0.75
config.gpu_options.visible_device_list = "0" #only the gpu 0 is allowed
set_session(tf.Session(config=config))

from keras_utils import Tensorboard_with_write_batch_logs
from keras.callbacks import CSVLogger, ModelCheckpoint, TensorBoard
from keras import optimizers


def get_init_epoch(weights_path):
    checkpoint_name = weights_path.split('/')[-1]
    return int(checkpoint_name.split('-')[2])


def train_model(batch_size, weights_path, optimizer, lr, job_dir, train_epocs, data_path, train_csv, val_csv, image_dir):
    logging.getLogger().setLevel(logging.INFO)
    batch_size *= 3
    is_full_data = False
    initial_epoch = 0
    _loss_tensor = contrastive_loss_fn(batch_size=batch_size)
    accuracy = accuracy_fn(batch_size)

    # Create paths
    paths = [os.path.join(job_dir, "output"), os.path.join(job_dir, "logs")]
    for path in paths:
        if not os.path.exists(path):
            os.makedirs(path)

    if weights_path:
        model = ranknet()
        loaded_weight_path = os.path.join(job_dir, "loaded_weight.h5")
        with file_io.FileIO(weights_path, mode='rb') as input_f:
            with file_io.FileIO(loaded_weight_path, mode='wb+') as output_f:
                wt_read = input_f.read()
                output_f.write(wt_read)
        model = tf.keras.models.load_model(loaded_weight_path)
        initial_epoch = get_init_epoch(weights_path)
        logging.info('Loaded the model from: {}, initial epoch: {}'.format(weights_path, initial_epoch))
        return

    else:
        # Build model, accuracy, loss tensors
        logging.info("Building Model: {}".format('ranknet'))
        model = ranknet()

        # set optimizer and compile model
        if optimizer == "mo":
            optm = tf.keras.optimizers.SGD(learning_rate=lr, momentum=0.9, nesterov=True, name='SGD')
            model.compile(loss=_loss_tensor,
                          optimizer=optm,#tf.train.MomentumOptimizer(learning_rate=lr, momentum=0.9, use_nesterov=True),
                          metrics=[accuracy])
        elif optimizer == "rms":
            model.compile(loss=_loss_tensor, optimizer=tf.train.RMSPropOptimizer(lr), metrics=[accuracy])
        else:
            logging.error("Optimizer not supported")
            return

    # Batch generator
    img_width, img_height = [int(v) for v in model.input[0].shape[1:3]]
    dg = DataGenerator({
        "rescale": 1. / 255,
        "horizontal_flip": True,
        "vertical_flip": True,
        "zoom_range": 0.2,
        "shear_range": 0.2,
        "rotation_range": 30,
        "fill_mode": 'nearest'
    }, data_path, train_csv, val_csv, image_dir, target_size=(img_width, img_height))
    train_generator = dg.get_train_generator(batch_size, is_full_data)
    test_generator = dg.get_test_generator(batch_size)

    # set callbacks
    csv_logger = CSVLogger(os.path.join(job_dir, "output/training.log"))
    model_checkpoint_path = "weights-improvement-{epoch:02d}-{val_loss:.2f}.h5"
    model_checkpointer = ModelCheckpoint(os.path.join(job_dir, model_checkpoint_path), save_best_only=True, save_weights_only=False,
                                         verbose=1, period=2)
    #tensorboard = TensorBoard(log_dir=job_dir + '/logs/', histogram_freq=0, write_graph=True, write_images=True)
    tensorboard = Tensorboard_with_write_batch_logs(log_dir=job_dir + '/logs/', histogram_freq=0, write_graph=True, write_images=True, log_every=1000)
    callbacks = [csv_logger, model_checkpointer, tensorboard]

    model_json = model.to_json()
    write_file_and_backup(model_json, job_dir, "output/model.def")

    with tf.Session() as sess:
        sess.run(tf.global_variables_initializer())
        history = model.fit_generator(train_generator,
                                      steps_per_epoch=(train_generator.n // (train_generator.batch_size)),
                                      validation_data=test_generator,
                                      epochs=train_epocs,
                                      validation_steps=(test_generator.n // (test_generator.batch_size)),
                                      callbacks=callbacks,
                                      initial_epoch=initial_epoch)
        pd.DataFrame(history.history).to_csv(os.path.join(job_dir, "output/history.csv"))
        model.save_weights(os.path.join(job_dir,'output/final_model.h5'))


if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    parser.add_argument(
        '--batch_size',
        help='batch size',
        default=16,
        type=int
    )
    parser.add_argument(
        '--optimizer',
        help='Optimizer',
        default='mo',
        #required=True
    )
    parser.add_argument(
        '--lr',
        help='learning rate',
        default=0.01,
        type=float
    )
    parser.add_argument(
        '--train_epocs',
        help='number of epochs to train',
        default=50,
        type=int
    )
    parser.add_argument(
        '--data_path',
        help='Paths to training data, should contain images folder and triplets csv',
        default='dataset/kitchen_houston_1000/',
        #required=True
    )
    parser.add_argument(
        '--image_dir',
        help='Paths to training data, should contain images folder and triplets csv',
        default='dataset/kitchen_houston_1000/images/',
        # required=True
    )
    parser.add_argument(
        '--job_dir',
        help='location to write checkpoints and export models',
        default='job_dir_ranknet/kitchen_houston_1000_1.3/',
        # required=True
    )
    parser.add_argument(
        '--weights-path',
        help='GCS location of pretrained weights path',
        default='job_dir_ranknet/kitchen_houston_1000_1.3/weights-improvement-02-0.26.h5'
    )
    parser.add_argument(
        '--train_csv',
        help='train csv file name',
        default='train.sample.csv'
    )
    parser.add_argument(
        '--val_csv',
        help='val csv file name',
        default='valid.sample.csv'
    )

    args = parser.parse_args()
    arguments = args.__dict__

    train_model(**arguments)
