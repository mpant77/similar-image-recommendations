import cv2
import time
import tqdm
import pandas as pd
import numpy as np
import keras.backend as K
from annoy import AnnoyIndex
from keras.models import model_from_json


def load_model(model_json_path, weights_path):
    with open(model_json_path, 'r') as f:
        model_json = f.read()
        model = model_from_json(model_json)
    print('Loading model with weights')
    model.load_weights(weights_path)
    print("Model Successfully loaded")
    return model


def get_image_list(val_csv_path):
    with open(val_csv_path, "r") as file:
        db = file.read().split("\n")
    images = set()
    query_images = set()
    for triplet in db:
        triplet = triplet.split(",")
        query_images.add("tops/" + triplet[0])
        for j in range(len(triplet)):
            images.add("tops/" + triplet[j])
    return images, query_images


def preprocess_img(image):
  p_image = cv2.resize(cv2.cvtColor(image, cv2.COLOR_BGR2RGB), (size, size))
  p_image = p_image.astype(K.floatx())
  p_image *= 1. / 255
  p_image = np.expand_dims(p_image, axis=0)
  return p_image


def get_batch(iterable, n=1):
    l = len(iterable)
    for ndx in tqdm(range(0, l, n)):
        yield iterable[ndx:min(ndx + n, l)]


def get_images(files):
  images = []
  for f in files:
    try:
      image = cv2.imread(f)
      image = preprocess_img(image)
      images.append(image)
    except:
      pass
  return images


def get_pred(model, image):
  t1=time()
  if model.input_shape[0]:
    op_quer = model.predict([image,image,image])
  else:
    op_quer = model.predict(image)
  inference_times.append(time()-t1)
  return op_quer


def get_feature_vectors(model, images, pred_csv_path):
    all_files, all_preds = [], []
    for files in get_batch(list(images), n=32):
        pp_images = get_images(files)
        batch_x = np.zeros((len(pp_images), 224, 224, 3), dtype=K.floatx())
        for i, x in enumerate(pp_images):
            batch_x[i] = x
        y_pred = get_pred(model, batch_x)
        all_files.extend(files)
        all_preds.extend(y_pred)
    preds_df = pd.DataFrame(all_preds, index=all_files)
    preds_df.to_csv(pred_csv_path)
    return all_files, all_preds, preds_df


def build_annoy_model(preds_df, all_files, all_preds):
    annoy_model = AnnoyIndex(len(preds_df.columns))
    counter = 0
    for filepath, features in zip(all_files, all_preds):
        annoy_model.add_item(counter, features)
        counter += 1

    annoy_model.build(84)
    annoy_filename = "tops_val_full-annoy.ann"
    annoy_model.save(annoy_filename)