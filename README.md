# similar-image-recommendations

image-similarity-ranking:

This approach is based on the idea of google deep ranking technique and a research paper
Learning Fine-grained Image Similarity with Deep Ranking ( https://arxiv.org/abs/1404.4661 ).

We have used keras, tensorflow to train a self defined model along with vgg16 convolution net.
Model is used to generate image embeddings which represents images in relevance to visual similarity.
These embeddings are used to search and rank similar images to a given query image.


